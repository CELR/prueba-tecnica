# Prueba técnica EMQU

## Tecnologías utilizadas

- Angular 2+
- Laravel 10
- MariaDB

## Instalación

En la carpeta raiz ejecutar docker compose
```
docker-compose up -d --build
```
esperar a que termine de crear los contenedores y esten activos, se pueden verificar accediendo por el navegador con 
- http://localhost:4200 para el frontend
- http://localhost:8000 para el backend
puede tardar un poco en conectar
luego ejecutar la migracion de la base de datos para que pueda funcionar correctamente el backend
```
docker-compose exec backend php artisan migrate
```
- http://localhost:8000/phpmyadmin/index.php para ver la base de datos

Al entrar en la url del frontend apareceran en la pantalla del login

El usuario y la contraseña
Email: email@test.com
Password: pass1234

Temporalmente se puede probar en linea con el link: http://165.227.97.45:4200/