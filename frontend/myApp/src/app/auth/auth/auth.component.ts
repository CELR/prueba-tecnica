import { NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { FormControl, FormGroupDirective, FormsModule, NgForm, ReactiveFormsModule, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent {
  constructor(private router: Router){

  }

  email!: string
  password!: string

  login(){
    if(this.email === 'email@test.com' && this.password == 'pass1234'){
      localStorage.setItem('session', 'authenticated');
      this.router.navigateByUrl('/dashboard/equipment');
    }
  }
}
