import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth/auth.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { authGuardGuard } from './auth/guards/auth-guard.guard';

const routes: Routes = [
  { path:'', redirectTo:'/login', pathMatch:'full'},
  { path:'login', component:AuthComponent},
  { path:'dashboard', component: DashboardComponent,
    canActivate: [authGuardGuard],
    children: [{
      path: 'equipment', loadChildren:  () => import('./equipment/equipment.module').then(m => m.EquipmentModule)
    }]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
