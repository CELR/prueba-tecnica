import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipmentCreateComponent } from './equipment-create/equipment-create.component';
import { EquipmentListComponent } from './equipment-list/equipment-list.component';
import { EquipmentEditComponent } from './equipment-edit/equipment-edit.component';
import { EquipmentRoutingModule } from './equipment-routing.module';
import {MatCardModule} from '@angular/material/card';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { DashboardModule } from '../dashboard/dashboard.module';
import { StatisticsComponent } from './statistics/statistics.component';



@NgModule({
  declarations: [
    EquipmentCreateComponent,
    EquipmentListComponent,
    EquipmentEditComponent,
    StatisticsComponent
  ],
  imports: [
    CommonModule,
    DashboardModule,
    EquipmentRoutingModule,
    MatCardModule,
    FormsModule,
    MatButtonModule,
  ]
})
export class EquipmentModule { }
