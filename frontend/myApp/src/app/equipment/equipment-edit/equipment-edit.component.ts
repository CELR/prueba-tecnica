import { Component } from '@angular/core';
import { EquipmentService } from '../equipment.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-equipment-edit',
  templateUrl: './equipment-edit.component.html',
  styleUrls: ['./equipment-edit.component.scss']
})
export class EquipmentEditComponent {
  constructor(private equipmentService: EquipmentService, private route: ActivatedRoute){}

  id!: number
  ipv4!: string
  name!: string
  errors: any = []
  success: boolean = false;
  isLoading = false;

  ngOnInit(){
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.getEquipment()
  }

  getEquipment(){
    this.isLoading = true;
    this.equipmentService.getEquipment(this.id).subscribe({
      next: (res:any)=>{
        this.isLoading = false;
        this.name = res.name;
        this.ipv4 = res.ipv4
      }
    })
  }

  updateEquipment(){
    this.isLoading = true;
    this.success = false;
    this.errors = [];
    const inputData = {
      name: this.name,
      ipv4: this.ipv4
    }
    this.equipmentService.updateEquipment(inputData, this.id).subscribe({
      next: (res:any) => {
        this.isLoading = false;
        if(res['success']) this.success = true
        if(res['errors']) this.errors = res['errors']
      },
      error: (err) => {
        this.isLoading = false;
        console.log(err)

      }
    });
  }
}
