import { Component } from '@angular/core';
import { EquipmentResponse, EquipmentService } from '../equipment.service';

@Component({
  selector: 'app-equipment-list',
  templateUrl: './equipment-list.component.html',
  styleUrls: ['./equipment-list.component.scss']
})
export class EquipmentListComponent {
  isLoading = false
  equipments!: EquipmentResponse[];

  constructor(private equipmentService: EquipmentService ){}

  ngOnInit(){
    this.getEquipmentList()
  }

  getEquipmentList(){
    this.isLoading = true;
    this.equipmentService.getEquipmentList().subscribe({
      next: (res:any) => {
        this.isLoading = false
        this.equipments = res;
        console.log(res)
      },
      error: (err) => {
        this.isLoading = false
        console.log(err)
      }
    });
  }

  pingEquipment(name:any, ipv4:any){
    this.isLoading = true;
    this.equipmentService.pingEquipment({name,ipv4}).subscribe({
      next: (res:any) => {
        this.isLoading = false
        alert(res.success)
        console.log(res)
      },
      error: (err) => {
        this.isLoading = false
        console.log(err)
      }
    })
  }

  deleteEquipment(id:any){
    this.isLoading = true;
    this.equipmentService.deleteEquipment(id).subscribe({
      next: (res:any) => {
        this.isLoading = false
        this.getEquipmentList()
        console.log(res)
      },
      error: (err) => {
        this.isLoading = false
        console.log(err)
      }
    })
  }
}
