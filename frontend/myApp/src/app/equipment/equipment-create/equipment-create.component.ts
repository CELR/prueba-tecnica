import { Component } from '@angular/core';
import { EquipmentService } from '../equipment.service';

@Component({
  selector: 'app-equipment-create',
  templateUrl: './equipment-create.component.html',
  styleUrls: ['./equipment-create.component.scss']
})
export class EquipmentCreateComponent {

  constructor(private equipmentService: EquipmentService){}


  ipv4!: string
  name!: string
  errors: any = []
  success: boolean = false;
  isLoading = false;
  saveEquipment(){
    this.isLoading = true;
    this.success = false;
    this.errors = [];
    const inputData = {
      name: this.name,
      ipv4: this.ipv4
    }
    this.equipmentService.saveEquipment(inputData).subscribe({
      next: (res:any) => {
        this.isLoading = false;
        if(res['success']) this.success = true
        if(res['errors']) this.errors = res['errors']
      },
      error: (err) => {
        this.isLoading = false;
        console.log(err)

      }
    });
  }
}
