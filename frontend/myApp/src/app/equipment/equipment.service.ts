import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export interface EquipmentResponse {
  id?: number;
  name: string;
  ipv4: string;
}

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {
  baseURL = 'http://localhost:8000'
  constructor(private httpClient: HttpClient) {
  }

  saveEquipment(inputData: object){
    return this.httpClient.post(`${this.baseURL}/equipment`, inputData);
  }

  updateEquipment(inputData: object, id: number){
    return this.httpClient.post(`${this.baseURL}/equipment/${id}`, inputData);
  }

  deleteEquipment(id: number){
    return this.httpClient.post(`${this.baseURL}/equipment/delete/${id}`,{});
  }

  getEquipmentList(){
    return this.httpClient.get(`${this.baseURL}/equipment`)
  }

  getStatistics(){
    return this.httpClient.get(`${this.baseURL}/equipment/ping/statistics`)
  }

  getEquipment(id: number){
    return this.httpClient.get(`${this.baseURL}/equipment/${id}`)
  }

  pingEquipment(inputData: object){
    return this.httpClient.post(`${this.baseURL}/equipment/ping`, inputData);
  }

}
