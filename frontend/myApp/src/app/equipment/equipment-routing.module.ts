import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EquipmentListComponent } from './equipment-list/equipment-list.component';
import { EquipmentCreateComponent } from './equipment-create/equipment-create.component';
import { EquipmentEditComponent } from './equipment-edit/equipment-edit.component';
import { StatisticsComponent } from './statistics/statistics.component';


const routes: Routes = [
  { path:'', component:EquipmentListComponent},
  { path:'create', component:EquipmentCreateComponent},
  { path:'statistics', component:StatisticsComponent},
  { path:'edit/:id', component:EquipmentEditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipmentRoutingModule { }
