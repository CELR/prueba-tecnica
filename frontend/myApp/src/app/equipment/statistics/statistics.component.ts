import { Component } from '@angular/core';
import { EquipmentService } from '../equipment.service';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent {
  isLoading = false
  equipments:any = []

  constructor(private equipmentService: EquipmentService ){}

  ngOnInit(){
    this.getStatistics()
  }


  getStatistics(){
    this.isLoading = true;
    this.equipmentService.getStatistics().subscribe({
      next: (res:any) => {
        this.isLoading = false
        this.equipments = res;
        console.log(res)
      },
      error: (err) => {
        this.isLoading = false
        console.log(err)
      }
    });
  }
}
