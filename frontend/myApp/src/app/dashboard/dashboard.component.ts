import { ChangeDetectorRef, Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  constructor(private breakpointObserver: BreakpointObserver, private changeDetector: ChangeDetectorRef, private router: Router){

  }

  title = 'Frontend Prueba tecnica EMQU';

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav


  ngAfterViewInit(){
    this.breakpointObserver.observe(['(max-width: 767.98px)']).subscribe((resp:any) => {
      if(resp.matches){
        this.sidenav.mode = 'over';
        this.sidenav.close();
      }else{
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }


    })

    this.changeDetector.detectChanges()
  }

  logout(){
    localStorage.removeItem('session')
    this.router.navigateByUrl('/login')
  }
}
