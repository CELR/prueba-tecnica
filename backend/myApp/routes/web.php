<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EquipmentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return json_encode(["welcome" => "user"]);
});

Route::post('/equipment',[EquipmentController::class,'createEquipment']);
Route::post('/equipment/ping',[EquipmentController::class,'pingEquipment']);
Route::get('/equipment/ping/statistics',[EquipmentController::class,'getPingCount']);
Route::get('/equipment',[EquipmentController::class,'getEquipmentList']);
Route::get('/equipment/{id}',[EquipmentController::class,'getEquipment']);
Route::post('/equipment/{id}',[EquipmentController::class,'updateEquipment']);
Route::post('/equipment/delete/{id}',[EquipmentController::class,'deleteEquipment']);
