<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\EquipmentRepository;
class EquipmentController extends Controller
{
    //

    public function __construct(
        protected EquipmentRepository $equipmentRepository,
    ) {}

    public function createEquipment(Request $request){
        return $this->equipmentRepository->storeEquipment($request->name, $request->ipv4);
    }

    public function updateEquipment(Request $request){
        return $this->equipmentRepository->updateEquipment($request->id,$request->name, $request->ipv4);
    }

    public function getEquipmentList(){
        return $this->equipmentRepository->getEquipments();
    }

    public function getEquipment(Request $request){
        return $this->equipmentRepository->getEquipment($request->id);
    }

    public function deleteEquipment(Request $request){
        return $this->equipmentRepository->deleteEquipment($request->id);
    }

    public function pingEquipment(Request $request){
        exec("ping -c 4 " . $request->ipv4, $output, $result);
        if ($result == 0) {
            \App\Models\Ping::create([
                'ipv4' => $request->ipv4,
                'successfull_ping' => 1
            ]);
            return ['success' => 'ping successfull'];
        }
        else{
            \App\Models\Ping::create([
                'ipv4' => $request->ipv4,
                'successfull_ping' => 0
            ]);
            return ['success' => 'ping unsuccessfull'];
        }
    }

    public function getPingCount(){
        $pingResults = DB::table('ping')->select(
            'ipv4',
            DB::raw("count(case when successfull_ping = 1 then 1 end) as successfull"),
            DB::raw("count(case when successfull_ping = 0 then 1 end) as unsuccessfull")
        )
        ->groupBy('ipv4')
        ->get();
        return $pingResults;
    }
}
