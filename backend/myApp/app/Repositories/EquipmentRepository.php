<?php

namespace App\Repositories;

use App\Models\Equipment as EquipmentModel;
use App\Entities\EquipmentEntity;

class EquipmentRepository
{

    private $errors = [];

    public function checkDuplicatedInputs($name,$ipv4, $id=null){
        $duplicateFields = [];
        $existingEquipment = EquipmentModel::where('name', $name)->orWhere('ipv4',$ipv4)->get()->where('id','!=',$id)->first();
        if($existingEquipment && $existingEquipment->id != $id ){
            if($existingEquipment->name == $name) $duplicateFields['name'] = 'duplicated name';
            if($existingEquipment->ipv4 == $ipv4) $duplicateFields['ipv4'] = 'duplicated ipv4';
        }
        return $duplicateFields;
    }

    public function storeEquipment($name, $ipv4){
        $equipment = new EquipmentEntity($name, $ipv4);
        if($equipment->validate()){
            $duplicatedInputs = $this->checkDuplicatedInputs($name,$ipv4);
            if(count($duplicatedInputs)) return ['errors' => $duplicatedInputs];
            $newEquipment = new EquipmentModel;
            $newEquipment->name = $name;
            $newEquipment->ipv4 = $ipv4;
            $newEquipment->save();
            return  ['success' => 'Equipment have been store'];

        }
        return ['errors' => $equipment->getBadInputs()];

    }

    public function updateEquipment($id, $name, $ipv4){
        $equipment = new EquipmentEntity($name, $ipv4);
        if($equipment->validate()){
            $duplicatedInputs = $this->checkDuplicatedInputs($name,$ipv4,$id);
            if(count($duplicatedInputs)) return ['errors' => $duplicatedInputs];
            $newEquipment = EquipmentModel::find($id);
            $newEquipment->name = $name;
            $newEquipment->ipv4 = $ipv4;
            $newEquipment->save();
            return  ['success' => 'Equipment have been updated'];

        }
        return ['errors' => $equipment->getBadInputs()];
    }

    public function getEquipments(){
        return EquipmentModel::All();
    }

    public function getEquipment($id){
        return EquipmentModel::find($id);
    }

    public function deleteEquipment($id){
        $equipment = EquipmentModel::find($id);
        if($equipment){
            $equipment->delete();
            return ['success' => 'Equipment deleted'];
        }
        return ['error' => 'Equipment does not exists'];
    }
}
