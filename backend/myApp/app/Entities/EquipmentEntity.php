<?php

namespace App\Entities;

class EquipmentEntity
{
    //

    private $name;
    private $ipv4;
    private $badInputs = [];

    public function __construct($name, $ipv4){
        $this->name = $name;
        $this->ipv4 = $ipv4;
    }

    public function validate(){
        $isValidName = strlen(trim($this->name)) > 0;
        if(!$isValidName) $this->badInputs['name'] = 'invalid input name';
        $isValidIpv4 = filter_var($this->ipv4, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        if(!$isValidIpv4) $this->badInputs['ipv4'] = 'invalid input ipv4';
        return ($isValidName && $isValidIpv4);
    }

    public function getBadInputs(){
        return $this->badInputs;
    }
}
